//
//  MailComposer.swift
//  FoodPin
//
//  Created by Larry Ball on 6/8/15.
//  Copyright (c) 2015 Larry Ball. All rights reserved.
//

//
//  MessageComposer.swift
//  FoodPin
//
//  Created by Larry Ball on 6/8/15.
//  Copyright (c) 2015 Larry Ball. All rights reserved.
//

import Foundation
import MessageUI

let emailRecipients = ["larry@larryaball.net"] // for pre-populating the recipients list (optional, depending on your needs)

class MailComposer: NSObject, MFMailComposeViewControllerDelegate {
    
    // A wrapper function to indicate whether or not a text message can be sent from the user's device
    func canSendMail() -> Bool {
        return MFMailComposeViewController.canSendMail()
    }
    
    // Configures and returns a MFMessageComposeViewController instance
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self  //  Make sure to set this property to self, so that the controller can be dismissed!
        mailComposeVC.setToRecipients(emailRecipients) //takes AnyObject!
        mailComposeVC.setMessageBody("Just sending an email in-app using Swift!", isHTML: false)
        
        return mailComposeVC
    }
    
    // MFMailComposeViewControllerDelegate callback - dismisses the view controller when the user is finished with it
    func mailComposeViewController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}