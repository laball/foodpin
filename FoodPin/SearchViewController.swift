//
//  RestaurantTableViewController.swift
//  FoodPin
//
//  Created by Larry Ball on 5/31/15.
//  Copyright (c) 2015 Larry Ball. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UISearchBarDelegate {

    var searchBar: UISearchBar!

    var client: YelpClient!
    
    let yelpConsumerKey     = "qof7GcBQnVQoKOoVlRMNOw"
    let yelpConsumerSecret  = "i3b-CxkuQyuw8NNFQRnXtHZJhRQ"
    let yelpToken           = "KPePP8LgBP4N-hQpeciCGjLg-InMDd71"
    let yelpTokenSecret     = "BirsjwwLnetEIU0z_QqWiz8aaP0"
    
    var userLocation: UserLocation!

    var restaurant: Array<Restaurant> = []
    var offset: Int = 0
    var total: Int!
    let limit: Int = 20
    var lastResponse: NSDictionary!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.client = YelpClient(consumerKey: yelpConsumerKey, consumerSecret: yelpConsumerSecret, accessToken: yelpToken, accessSecret: yelpTokenSecret)

        self.userLocation = UserLocation()

        self.searchBar = UISearchBar()
        self.searchBar.delegate = self
        self.searchBar.placeholder = "e.g. cheesburger, pizza"
        self.navigationItem.titleView = self.searchBar
        
    }

    final func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.clearResults()
        self.performSearch(searchBar.text)
    }

    final func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.clearResults()
        }
    }

    final func performSearch(term: String, offset: Int = 0, limit: Int = 20) {
        self.searchBar.text = term
        self.searchBar.resignFirstResponder() // hide keyboard
        self.onBeforeSearch()
        self.client.searchWithTerm(term, parameters: self.getSearchParameters(), offset: offset, limit: 20, success: {
            (operation: AFHTTPRequestOperation!, response: AnyObject!) -> Void in
            let results = (response["businesses"] as! Array).map({
                (restaurant: NSDictionary) -> Restaurant in
                return Restaurant(dictionary: restaurant)
            })
            self.restaurant += results
            self.total = response["total"] as! Int
            self.lastResponse = response as! NSDictionary
            self.offset = self.restaurant.count
            self.onResults(self.restaurant, total: self.total, response: self.lastResponse)
        }, failure: {
            (operation: AFHTTPRequestOperation!, error: NSError!) -> Void in
            println(error)
        })
    }

    func getSearchParameters() -> Dictionary<String, String> {
        var parameters = [
            "ll": "\(userLocation.latitude),\(userLocation.longitude)"
        ]
        for (key, value) in YelpFilters.instance.parameters {
            parameters[key] = value
        }
        return parameters
    }

    func onBeforeSearch() -> Void {}

    func onResults(results: Array<Restaurant>, total: Int, response: NSDictionary) -> Void {}

    final func clearResults() {
        self.restaurant = []
        self.offset = 0
        self.onResultsCleared()
    }

    func onResultsCleared() -> Void {}

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        if segue!.destinationViewController is UINavigationController {
            let navigationController = segue!.destinationViewController as! UINavigationController
            if (navigationController.viewControllers[0] is DetailViewController){
                let controller = navigationController.viewControllers[0] as! DetailViewController
                
            }
        }
    }

    func synchronize(searchView: RestaurantTableViewController) {
        self.searchBar.text = searchView.searchBar.text
        self.restaurant = searchView.restaurant
        self.total = searchView.total
        self.offset = searchView.offset
        self.lastResponse = searchView.lastResponse

        if self.restaurant.count > 0 {
            self.onResults(self.restaurant, total: self.total, response: self.lastResponse)
        } else {
            self.onResultsCleared()
        }
    }

}