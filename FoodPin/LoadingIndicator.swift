//
//  LoadingIndicator.swift
//  Yelp
//
//  Created by Larry Ball on 5/31/2015.
//  Copyright (c) 2015 Larry Ball. All rights reserved.
//

import UIKit

class LoadingIndicator: UIView {

    @IBOutlet weak var logo: UIImageView!

    override func awakeFromNib() {
        self.animate()
    }

    func animate() {
        logo.image = UIImage.animatedImageNamed("AnimatedLogo", duration: 0.4)
    }

    func stopAnimating() {
        logo.image = UIImage(named: "Logo")
    }

}