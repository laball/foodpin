//
//  DetailViewController.swift
//  FoodPin
//
//  Created by Larry Ball on 5/15/15.
//  Copyright (c) 2015 Larry Ball. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    @IBOutlet var restaurantImageView:UIImageView!
    
    @IBOutlet var tableView:UITableView!
    
    var restaurant: Restaurant!

    override func viewDidLoad() {
        super.viewDidLoad()

        var imgUrl = restaurant.imageUrl
        if (imgUrl != nil){
            ImageLoader.sharedLoader.imageForUrl(imgUrl!, completionHandler:{(image: UIImage?, url: String) in
                self.restaurantImageView.image = image!
            })
        }
        
        self.tableView.backgroundColor = UIColor(red: 240.0/255.0, green: 240.0/255.0, blue: 240.0/255.0, alpha: 0.2)
        
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        
        title = self.restaurant.name as String!
        
        tableView.estimatedRowHeight = 36.0 // set height to existing prototype cell
        tableView.rowHeight = UITableViewAutomaticDimension //set to default iOS row height
        
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.hidesBarsOnSwipe = false
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! DetailTableViewCell
        
        //configure the cell
        cell.mapButton.hidden = true
    
        switch indexPath.row {
        case 0:
            cell.fieldLabel.text = "Name"
            cell.valueLabel.text = restaurant.name as String!
        case 1:
            cell.fieldLabel.text = "Type"
            cell.valueLabel.text = restaurant.displayCategories
        case 2:
            cell.fieldLabel.text = "Location"
            cell.valueLabel.text = restaurant.displayAddress
            cell.mapButton.hidden = false
        default:
            cell.fieldLabel.text = ""
            cell.valueLabel.text = ""
        }
        
        cell.backgroundColor = UIColor.clearColor()
        
        return cell
    }

    @IBAction func openURL(sender: AnyObject){
        var url:NSURL = NSURL(string: restaurant.mobileUrl as String)!
        UIApplication.sharedApplication().openURL(url)
    }
    
    @IBAction func callNumber(sender: AnyObject){
        var url:NSURL = NSURL(string: restaurant.phoneNumber as String)!
            UIApplication.sharedApplication().openURL(url)
    }
    
    // following method allows segue to be unwound
    @IBAction func close(segue:UIStoryboardSegue) {
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "showMap" {
            let destinationController = segue.destinationViewController as! MapViewController
            destinationController.restaurant = restaurant
        }
        if segue.identifier == "showShare" {
            let destinationController = segue.destinationViewController as! ShareViewController
            destinationController.restaurant = restaurant
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
}