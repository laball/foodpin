//
//  CustomTableViewCell.swift
//  FoodPin
//
//  Created by Larry Ball on 5/14/15.
//  Copyright (c) 2015 Larry Ball. All rights reserved.
//

import Foundation

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var ratingImage: UIImageView!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var categoriesLabel: UILabel!
    @IBOutlet weak var dealsImage: UIImageView!
}