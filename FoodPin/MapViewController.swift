//
//  MapViewController.swift
//  FoodPin
//
//  Created by Larry Ball on 5/17/15.
//  Copyright (c) 2015 Larry Ball. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet var mapView:MKMapView!
    
    var restaurant: Restaurant!
    let regionRadius: CLLocationDistance = 1000

    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        mapView.delegate = self

        centerMapOnLocation(restaurant.geolocation)
        
        // Convert address to coordinate and annotate on map

        var coordinates = CLLocationCoordinate2DMake(restaurant.latitude!, restaurant.longitude!)
        var placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
                    
        // Add Annotation
        let annotation = MKPointAnnotation()
        annotation.title = self.restaurant.name as String

        annotation.subtitle = restaurant.shortAddress
        annotation.coordinate = coordinates
                    
        self.mapView.showAnnotations([annotation], animated: true)
        self.mapView.selectAnnotation(annotation, animated: true)
    }
    
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        let identifier = "MyPin"
        
        if annotation.isKindOfClass(MKUserLocation) {
            return nil
        }
        
        // Reuse the annotation if possible
        var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView.canShowCallout = true
        }
        
        let leftIconView = UIImageView(frame: CGRectMake(0, 0, 53, 53))

        var imgUrl = restaurant.imageUrl
        if (imgUrl != nil){
            ImageLoader.sharedLoader.imageForUrl(imgUrl!, completionHandler:{(image:     UIImage?, url: String) in leftIconView.image = image!
            })
        }
        
        annotationView.leftCalloutAccessoryView = leftIconView
        
        return annotationView
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
