//
//  ShareViewController.swift
//  FoodPin
//
//  Created by Larry Ball on 5/17/15.
//  Copyright (c) 2015 Larry Ball. All rights reserved.
//

import UIKit
import Social
import MessageUI

class ShareViewController: UIViewController, MFMailComposeViewControllerDelegate {

    var restaurant: Restaurant!
    
    @IBOutlet weak var backgroundImageView:UIImageView!
    
    @IBOutlet weak var facebookButton:UIButton!
    @IBOutlet weak var twitterButton:UIButton!
    @IBOutlet weak var messageButton:UIButton!
    @IBOutlet weak var emailButton:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Apply blurring effect
        var blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        var blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        backgroundImageView.addSubview(blurEffectView)
        
        // Move the buttons off screen (bottom)
        let translateDown = CGAffineTransformMakeTranslation(0, 500)
        facebookButton.transform = translateDown
        messageButton.transform = translateDown
        
        // Move the buttons off screen (top)
        let translateUp = CGAffineTransformMakeTranslation(0, -500)
        twitterButton.transform = translateUp
        emailButton.transform = translateUp
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        
        let translate = CGAffineTransformMakeTranslation(0, 0)
        facebookButton.hidden = false
        twitterButton.hidden = false
        emailButton.hidden = false
        messageButton.hidden = false
        
        UIView.animateWithDuration(0.8, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: nil, animations: {
            
            self.facebookButton.transform = translate
            self.emailButton.transform = translate
            
            }, completion: nil)
        
        UIView.animateWithDuration(0.8, delay: 0.5, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: nil, animations: {
            
            self.twitterButton.transform = translate
            self.messageButton.transform = translate
            
            }, completion: nil)
        
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    @IBAction func facebookButtonTapped(sender: UIButton){
        let textToShare = restaurant.name + " is awesome!  Check out this website about it! "
        
        let url:String = restaurant.mobileUrl as String
        
//        if(FBSDKAccessToken.currentAccessToken() != nil){
            if(SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook)){
            // User is already logged in
            var facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSheet.setInitialText(textToShare + url)

            var restaurantImageView:UIImage!
            var imgUrl = restaurant.imageUrl
            if (imgUrl != nil){
                ImageLoader.sharedLoader.imageForUrl(imgUrl!, completionHandler:{(image: UIImage?, url: String) in
                    restaurantImageView = image!
                })
            }

            facebookSheet.addImage(restaurantImageView)
            self.presentViewController(facebookSheet, animated: true, completion: nil)
        }else{
//            performSegueWithIdentifier("showFBLogin", sender: nil)
                var alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
        }
     }
    
    @IBAction func twitterButtonTapped(sender: UIButton){
        let textToShare = restaurant.name + " is awesome!  Check out this website about it! "
        
        let url:String = restaurant.mobileUrl as String
        
        //        if(FBSDKAccessToken.currentAccessToken() != nil){
        if(SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)){
            // User is already logged in
            var tweetSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            tweetSheet.setInitialText(textToShare + url)
            
            var restaurantImageView:UIImage!
            var imgUrl = restaurant.imageUrl
            if (imgUrl != nil){
                ImageLoader.sharedLoader.imageForUrl(imgUrl!, completionHandler:{(image: UIImage?, url: String) in
                    restaurantImageView = image!
                })
            }
            
            tweetSheet.addImage(restaurantImageView)
            self.presentViewController(tweetSheet, animated: true, completion: nil)
        }else{
            //            performSegueWithIdentifier("showFBLogin", sender: nil)
            var alert = UIAlertController(title: "Accounts", message: "Please login to a Twitter account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
//    let messageComposer = MessageComposer()
//    
//    @IBAction func sendTextMessageButtonTapped(sender: UIButton) {
//        // Make sure the device can send text messages
//        if (messageComposer.canSendText()) {
//            // Obtain a configured MFMessageComposeViewController
//            let messageComposeVC = messageComposer.configuredMessageComposeViewController()
//            
//            // Present the configured MFMessageComposeViewController instance
//            // Note that the dismissal of the VC will be handled by the messageComposer instance,
//            // since it implements the appropriate delegate call-back
//            presentViewController(messageComposeVC, animated: true, completion: nil)
//        } else {
//            // Let the user know if his/her device isn't able to send text messages
//            let errorAlert = UIAlertView(title: "Cannot Send Text Message", message: "Your device is not able to send text messages.", delegate: self, cancelButtonTitle: "OK")
//            errorAlert.show()
//        }
//    }
    @IBAction func sendTextMessageButtonTapped(sender: AnyObject) {
        let messageComposeViewController = configuredMessageComposeViewController()
        if MFMessageComposeViewController.canSendText(){
            self.presentViewController(messageComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMessageErrorAlert()
        }
    }
    
    func configuredMessageComposeViewController() -> MFMessageComposeViewController{
        
        let messageComposerVC = MFMessageComposeViewController()
        
        messageComposerVC.navigationBar.tintColor = UIColor.whiteColor()
        messageComposerVC.body = "\(restaurant.name) \n \(restaurant.mobileUrl)"
        
        return messageComposerVC
    }
    
    func showSendMessageErrorAlert() {
        let sendMessageErrorAlert = UIAlertView(title: "Could Not Send text message", message: "Your device could not send text message.  Please check text message configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMessageErrorAlert.show()
    }
    
    // MARK: MFMessageComposeViewControllerDelegate Method
    func messageComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

//    let mailComposer = MailComposer()
//    @IBAction func sendEmailButtonTapped(sender: AnyObject) {
//        // Make sure the device can send text messages
//        if (mailComposer.canSendMail()) {
//            // Obtain a configured MFMailComposeViewController
//            let mailComposeVC = mailComposer.configuredMailComposeViewController()
//            
//            // Present the configured MFMailComposeViewController instance
//            // Note that the dismissal of the VC will be handled by the mailComposer instance,
//            // since it implements the appropriate delegate call-back
//            presentViewController(mailComposeVC, animated: true, completion: nil)
//        } else {
//            // Let the user know if his/her device isn't able to send email
//            let errorAlert = UIAlertView(title: "Cannot Send Email", message: "Your device is not able to send email.", delegate: self, cancelButtonTitle: "OK")
//            errorAlert.show()
//        }
//    }
    @IBAction func sendEmailButtonTapped(sender: AnyObject) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()

        mailComposerVC.navigationBar.tintColor = UIColor.whiteColor()

        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
//        mailComposerVC.setToRecipients(["someone@somewhere.com"])
        mailComposerVC.setSubject(restaurant.name)
        mailComposerVC.setMessageBody(restaurant.mobileUrl, isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // following method allows segue to be unwound
    @IBAction func close(segue:UIStoryboardSegue) {
    }
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
//        if segue.identifier == "showFBLogin" {
//            let destinationController = segue.destinationViewController as! FBLogin
//        }
//    }
    
}