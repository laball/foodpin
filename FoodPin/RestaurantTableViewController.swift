//
//  RestaurantTableViewController.swift
//  FoodPin
//
//  Created by Larry Ball on 5/14/15.
//  Copyright (c) 2015 Larry Ball. All rights reserved.
//

import UIKit
import CoreLocation

class RestaurantTableViewController: SearchViewController, UITableViewDataSource, UITableViewDelegate, FiltersViewDelegate {
    
    @IBOutlet var tableView: UITableView!
    
    var loadingIndicator: LoadingIndicator!
        
    override func viewDidLoad() {
        super.viewDidLoad()

        // Erase back button title
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
        
        setupTableView()
        
        self.tableView.addInfiniteScrollingWithActionHandler({
            self.performSearch(self.searchBar.text, offset: self.offset, limit: self.limit)
        })
        self.tableView.showsInfiniteScrolling = false
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("onUserLocation"), name: "UserLocation/updated", object: nil)
        self.userLocation.requestLocation()
        
        let loadingIndicator = (NSBundle.mainBundle().loadNibNamed("LoadingIndicator", owner: self, options: nil)[0] as! LoadingIndicator)
        self.loadingIndicator = loadingIndicator
        self.tableView.tableFooterView = loadingIndicator
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.hidesBarsOnSwipe = true
    }
    
    override func viewDidAppear(animated: Bool) {

        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()

        self.performSearch("food", offset: self.offset, limit: self.limit)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        tableView.reloadData()
    }
    func setupTableView() {
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 200.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
 
    func onUserLocation() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "UserLocation/updated", object: nil)
        if self.searchBar.text == "" {
            self.performSearch("Restaurants")
        }
    }
    
    override func onBeforeSearch() {
        self.loadingIndicator.animate()
    }
    
    override func onResults(results: Array<Restaurant>, total: Int, response: NSDictionary) {
        self.tableView.infiniteScrollingView.stopAnimating()
        self.tableView.showsInfiniteScrolling = restaurant.count < total
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.tableView.reloadData()
    }
    
    override func onResultsCleared() {
        self.loadingIndicator.stopAnimating()
        self.tableView.tableFooterView = self.loadingIndicator
        self.tableView.showsInfiniteScrolling = false
        self.tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell") as! CustomTableViewCell
        
        let restaurant = self.restaurant[indexPath.row]
        
        var imgUrl = restaurant.imageUrl
        
        if (imgUrl != nil){
            ImageLoader.sharedLoader.imageForUrl(imgUrl!, completionHandler:{(image: UIImage?, url: String) in
                cell.previewImage.image = image!
            })
        }
        
        cell.previewImage.layer.cornerRadius = cell.previewImage.frame.size.width / 2
        cell.previewImage.layer.masksToBounds = true
        
        cell.nameLabel.text = "\(indexPath.row + 1). \(restaurant.name)"
        cell.ratingImage.setImageWithURL(restaurant.ratingImageURL)
        
        let reviewCount = restaurant.reviewCount
        if (reviewCount == 1) {
            cell.reviewLabel.text = "\(reviewCount) review"
        } else {
            cell.reviewLabel.text = "\(reviewCount) reviews"
        }
        
        cell.addressLabel.text = restaurant.displayAddress
        cell.categoriesLabel.text = restaurant.displayCategories
        
        let distance = restaurant.geolocation.distanceFromLocation(self.userLocation.location)
        cell.distanceLabel.text = String(format: "%.1f mi", distance / 1609.344)
        cell.distanceLabel.sizeToFit()
        
        cell.dealsImage.hidden = restaurant.deals == nil
        
        cell.layoutIfNeeded()
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.restaurant.count
    }
    
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue!.identifier == "showRestaurantDetail" {
            /* In Swift, you can ￼use if and let together to work with optionals. The conditional block will only be executed if an indexPath exists. */
            if let indexPath = self.tableView.indexPathForSelectedRow() {
                let destinationController = segue!.destinationViewController as! DetailViewController
                destinationController.restaurant = restaurant[indexPath.row]
            }
        }
        if segue!.destinationViewController is UINavigationController {
            let navigationController = segue!.destinationViewController as! UINavigationController
            println(navigationController.viewControllers[0])
            if navigationController.viewControllers[0] is FiltersViewController {
                let controller = navigationController.viewControllers[0] as! FiltersViewController
                controller.delegate = self

            }
        }
    }

    final func onFiltersDone(controller: FiltersViewController) {
        println("FiltersViewDelegate")
        if self.searchBar.text != "" {
            self.clearResults();
            self.performSearch(self.searchBar.text)
        }
    }

}