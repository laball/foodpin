////
////  ReviewViewController.swift
////  FoodPin
////
////  Created by Larry Ball on 5/17/15.
////  Copyright (c) 2015 Larry Ball. All rights reserved.
////
//
//import UIKit
//
//class ReviewViewController: UIViewController{
//    
//    @IBOutlet weak var backgroundImageView:UIImageView!
//    @IBOutlet weak var dialogView:UIView!
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        // Apply blurring effect
//        var blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
//        var blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.frame = view.bounds
//        backgroundImageView.addSubview(blurEffectView)
//        
//        // Combining scale and translate transforms
//        let scale = CGAffineTransformMakeScale(0.0, 0.0)
//        let translate = CGAffineTransformMakeTranslation(0, 500)
//        dialogView.transform = CGAffineTransformConcat(scale, translate)
//    }
//    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    
//    override func viewDidAppear(animated: Bool) {
//        
//        UIView.animateWithDuration(0.7, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.5, options: nil, animations: {
//            
//            let scale = CGAffineTransformMakeScale(1, 1)
//            let translate = CGAffineTransformMakeTranslation(0, 0)
//            self.dialogView.transform = CGAffineTransformConcat(scale, translate)
//            
//            }, completion: nil)
//        
//    }
//    
//    override func prefersStatusBarHidden() -> Bool {
//        return true
//    }
//    
//}
